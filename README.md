# Nonut

A C++ wrapper for the Gothic 2 Online API 🌰🔫💀

## Table of contents

- [Nonut](#nonut)
  - [Table of contents](#table-of-contents)
  - [Introduction](#introduction)
  - [Usage requirements](#usage-requirements)
  - [Milestones](#milestones)

## Introduction

This project aims to wrap the Squirrel API exposed by the Gothic 2 Online into c++ code base executed in a module.
[Template Gothic 2 Online Module](https://gitlab.com/GothicMultiplayerTeam/modules/squirrel-template)

## Usage requirements

NoNut uses C++ 20 standard. Please make sure you have a toolchain that supports it.

Checkout this repository into ``your_module_name/dependencies/`` in you module folder. After this step you should be able to access nonut folder ``your_module_name/dependencies/nonut/``

To load this dependency into your Module you need to add following lines to your main CMakeLists.txt file:

```cmake
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED True)
```

Make sure that your dependencies CMakeLists.txt looks like this:

```cmake
add_subdirectory(squirrel)
add_subdirectory(sqrat)
# We add our repo directory here
add_subdirectory(nonut)

# ...

# It is advised to have two separate targets for the client and the server module
# Unless you are working only on the one side.
target_link_libraries(squirrel-template PUBLIC Squirrel)
target_link_libraries(squirrel-template PUBLIC SqRat)
# In case you want Client Wrapper API
target_link_libraries(squirrel-template PUBLIC NoNut_Client)
# In case you want Server Wrapper API
target_link_libraries(squirrel-template PUBLIC NoNut_Server)

# ...
```

To initialize NoNut in your module do the following:

```cpp
#include "NoNutInitClient.h"
#include "NoNutInitServer.h"

extern "C" SQRESULT SQRAT_API sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
	/// YOU CAN ONLY USE CLIENT OR SERVER! NOT BOTH!
	/// NoNut will crash when called in the wrong client/server context.
	/// Make sure you are using correct things. :)

	// Initialize client
	nonut::g2o::NoNutInitClient();

	// Initialize server
	nonut::g2o::NoNutInitServer();
}
```

## Milestones

NoNut is currently based on Gothic 2 Online 0.2.1.
API needs to be updated in the future to match the newer versions.

TODO:
___
- [ ] Return type handling
  - [x] Simple types
  - [x] Class types
  - [ ] Array types
  - [x] Slot types
  - [x] Pointer types
___
- [x] Classes
  - [x] Create object instances
  - [x] Copy objects instances
  - [x] Static class support  
___
- [ ] API binded
  - [ ] Client API (80%)
  - [ ] Server API (20%)
  - [ ] Shared API (10%)
___
- [x] Function calls
- [x] Binding events
- [x] Constants
___
- [ ] Migrate to 0.3.X (when available)
- [ ] Examples
- [ ] Documentation
___
#ifndef NONUT_G2O_SERVER_CONSTANT_SEVER_CONSTANTS_H
#define NONUT_G2O_SERVER_CONSTANT_SEVER_CONSTANTS_H

#include "CommonHeader.h"
namespace nonut::g2o
{
	class ServerConstants
	{
	public:
		static void init();

		// AntyCheat

		/// @brief Represents speed hack state.
		static inline Int AC_SPEED_HACK = 0;

		// Network

		/// @brief disconnected player state.
		static inline Int DISCONNECTED = 0;
		/// @brief lost connection player state.
		static inline Int LOST_CONNECTION = 0;
		/// @brief crash player state.
		static inline Int HAS_CRASHED = 0;

		// Talent

		/// @brief Represents npc sneak talent.
		static inline Int TALENT_SNEAK = 0;
		/// @brief Represents npc picklock talent.
		static inline Int TALENT_PICK_LOCKS = 0;
		/// @brief Represents npc pickpocket talent.
		static inline Int TALENT_PICKPOCKET = 0;
		/// @brief Represents npc runes creation talent.
		static inline Int TALENT_RUNES = 0;
		/// @brief Represents npc potion creation talent.
		static inline Int TALENT_ALCHEMY = 0;
		/// @brief Represents npc smith talent.
		static inline Int TALENT_SMITH = 0;
		/// @brief Represents npc trophy gathering talent.
		static inline Int TALENT_THROPHY = 0;
		/// @brief Represents npc acrobatic talent.
		static inline Int TALENT_ACROBATIC = 0;
		/// @brief Represents npc health regeneration talent.
		static inline Int TALENT_REGENERATE = 0;
		/// @brief Represents npc firemaster talent (unused by the game).
		static inline Int TALENT_FIREMASTER = 0;
		/// @brief Represents npc old pickpocket talent (unused by the game).
		static inline Int TALENT_PICKPOCKET_UNUSED = 0;
		/// @brief Represents npc talent A (unused by the game).
		static inline Int TALENT_A = 0;
		/// @brief Represents npc talent B (unused by the game).
		static inline Int TALENT_B = 0;
		/// @brief Represents npc talent C (unused by the game).
		static inline Int TALENT_C = 0;
		/// @brief Represents npc talent D (unused by the game).
		static inline Int TALENT_D = 0;
		/// @brief Represents npc talent E (unused by the game).
		static inline Int TALENT_E = 0;
	};
}
#endif // NONUT_G2O_SERVER_CONSTANT_SEVER_CONSTANTS_H

#ifndef NONUTSERVERINIT_H_
#define NONUTSERVERINIT_H_

#include "CommonHeader.h"
#include "constant/ServerConstants.h"

namespace nonut::g2o 
{
	inline void NoNutInitServer()
	{
		ServerEventHandlers::init();
		ServerConstants::init();
		SharedConstants::init();
	}
}
#endif // NONUTCLIENTINIT_H_

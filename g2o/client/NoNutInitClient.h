#ifndef NONUTCLIENTINIT_H_
#define NONUTCLIENTINIT_H_

#include "CommonHeader.h"

namespace nonut::g2o 
{
	inline void NoNutInitClient()
	{
		ClientEventHandlers::init();
		ClientConstants::init();
		SharedConstants::init();
	}
}
#endif // NONUTCLIENTINIT_H_

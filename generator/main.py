import os

from api import *
import json

from class_gen import ClassGenerator
from type_mapping import fill_class_set

disabled_classes = {"Daedalus", "DaedalusSymbol"}


def generate_classes(classes: List[Class]):
    for class_ in classes:
        if class_.definition.name in disabled_classes:
            continue
        generator = ClassGenerator(class_)
        generator.generate()
        class_folder = f"out/g2o/{class_.definition.side.lower()}/class/{class_.definition.category.lower()}/"
        os.makedirs(os.path.dirname(class_folder), exist_ok=True)
        with open(f'{class_folder}/{class_.definition.name}.h', 'w') as file:
            file.write(generator.header_code)

        with open(f'{class_folder}/{class_.definition.name}.cpp', 'w') as file:
            file.write(generator.source_code)


def generate_constants(constants: List[Constant]):
    for constant in constants:
        print()


def generate_events(events: List[Event]):
    for event in events:
        print()


def generate_functions(functions: List[Function]):
    for function in functions:
        print()


def generate_globals(globals: List[Global]):
    for global_ in globals:
        print()


if __name__ == '__main__':
    with open('api.json', 'r') as f:
        data = json.load(f)

    u = Api.from_dict(data)
    fill_class_set(u.classes)
    generate_classes(u.classes)

from api import *
from base_gen import BaseGenerator
from type_mapping import map_type, class_set

class_include_placeholder = "///REPLACE_ME_WITH_CLASS_INCLUDES"


class ClassGenerator(BaseGenerator):
    def __init__(self, class_: Class):
        super().__init__()
        self.constructors = class_.constructors
        self.definition = class_.definition
        self.methods = class_.methods
        self.properties = class_.properties

        self.derived_class_type = None
        self.used_classes = set()
        self.repeated_names = set()

    def generate(self):
        BaseGenerator.generate(self)
        self.generate_header()
        self.generate_source()

    def generate_header(self):
        include_guard_name = f"NONUT_G2O_" \
                             f"{self.definition.side.upper()}_" \
                             f"CLASS_" \
                             f"{self.definition.name.upper()}_H"

        if self.definition.extends == "None":
            self.derived_class_type = "Class" if not self.definition.static else "StaticClass"
        else:
            self.derived_class_type = self.definition.extends[
                                      self.definition.extends.find('[') + len('['):self.definition.extends.rfind(']')]

        # Open include guard
        self._header_code += f"#ifndef {include_guard_name}\n"
        self._header_code += f"#define {include_guard_name}\n\n"
        self._header_code += "#include \"CommonHeader.h\"\n" \
                             "#include \"CustomTypes.h\"\n"
        self._header_code += f"#include \"{self.derived_class_type}.h\"\n"

        self._header_code += f"{class_include_placeholder}"

        # Open namespace
        self._header_code += "\nnamespace nonut::g2o\n{\n"

        # Open class
        self._header_code += f"class {self.definition.name} : " \
                             f"public {self.derived_class_type}\n" \
                             "{\npublic:\n"

        if not self.definition.static:
            # Constructors
            self.generate_header_constructors()

        # Methods
        self.generate_header_methods()
        # Properties
        self.generate_header_properties()

        # Static class stuff
        if self.definition.static:
            self._header_code += f"static {self.definition.name}* get();\n" \
                                 f"\nprivate:\n" \
                                 f"static inline {self.definition.name}* instance_ptr = nullptr;\n" \
                                 f"{self.definition.name}();\n"

        # Close class
        self._header_code += "};\n"
        # Close namespace
        self._header_code += "}\n"
        # Close include guard
        self._header_code += f"#endif // {include_guard_name}\n"

        additional_includes = ''
        for class_ in self.used_classes:
            additional_includes += f"#include \"class/{class_set[class_]}/{class_}.h\"\n"

        self._header_code = self.header_code.replace(class_include_placeholder, additional_includes)

    def generate_header_methods(self):
        for method in self.methods:
            # Open method
            if method.name in self.repeated_names:
                self._header_code += "//"
            else:
                self.repeated_names.add(method.name)

            if method.returns is None:
                self._header_code += "Function<void"
            else:
                if method.returns.obj_type in class_set:
                    self.used_classes.add(method.returns.obj_type)

                self._header_code += f"Function<{map_type(method.returns.obj_type)}"
            i = False
            for param in method.params:
                self._header_code += ", "
                if param.obj_type in class_set:
                    self.used_classes.add(param.obj_type)
                self._header_code += f"{map_type(param.obj_type)}"
            # Close method
            self._header_code += f"> {method.name};\n"

    def generate_header_constructors(self):
        for constructor in self.constructors:
            # Open ctor
            self._header_code += f"{self.definition.name}("
            i = False
            for param in constructor.params:
                if i:
                    self._header_code += ", "
                i = True
                if param.obj_type in class_set:
                    self.used_classes.add(param.obj_type)
                self._header_code += f"{map_type(param.obj_type)} {param.name}"
            # Close ctor
            self._header_code += ");\n"
        self._header_code += f"{self.definition.name}(const SQObject object, const String& className = \"{self.definition.name}\");\n"
        self._header_code += f"COPY_CTOR({self.definition.name});\n"

    def generate_header_properties(self):
        for property_ in self.properties:
            if property_.returns.obj_type in class_set:
                self.used_classes.add(property_.returns.obj_type)
            self._header_code += f"Property<{map_type(property_.returns.obj_type)}"
            if property_.read_only:
                self._header_code += ", true"
            self._header_code += f"> {property_.name};\n"

    def generate_source(self):
        self._source_code += "#undef min\n#undef max\n\n"
        self._source_code += f"#include \"{self.definition.name}.h\"\n\n"
        # Open namespace
        self._source_code += "namespace nonut::g2o\n{\n"

        for constructor in self.constructors:
            self._source_code += f"{self.definition.name}::{self.definition.name}("
            i = False
            for param in constructor.params:
                if i:
                    self._source_code += ", "
                i = True
                self._source_code += f"{map_type(param.obj_type)} {param.name}"

            self._source_code += f") : {self.definition.name}(SQ_NULL)\n" \
                                 "{\n"
            self._source_code += "classCtor("
            j = False
            for param in constructor.params:
                if j:
                    self._source_code += ", "
                j = True
                self._source_code += f"{param.name}"

            self._source_code += ");\n}\n\n"

        if self.derived_class_type == "StaticClass":
            self._source_code += f"{self.definition.name}* {self.definition.name}::get()\n" \
                                 "{\nif (instance_ptr == nullptr)\n{\n" \
                                 f"instance_ptr = new {self.definition.name}();\n" \
                                 "}\nreturn instance_ptr;\n}\n\n"

            self._source_code += f"{self.definition.name}::{self.definition.name}() :\n" \
                                 f"{self.derived_class_type}(\"{self.definition.name}\")"
        elif self.derived_class_type == "Class":
            self._source_code += f"{self.definition.name}::{self.definition.name}(" \
                                 f"const SQObject object, const String& className) :\n" \
                                 f"{self.derived_class_type}(className, object)"
        else:
            self._source_code += f"{self.definition.name}::{self.definition.name}(" \
                                 f"const SQObject object, const String& className) :\n" \
                                 f"{self.derived_class_type}(object, className)"

        for method_name in self.repeated_names:
            self._source_code += f",\nMETHOD_CTOR({method_name})"

        for property in self.properties:
            self._source_code += f",\nPROPERTY_CTOR({property.name})"

        self._source_code += "\n{\n}"

        # Close namespace
        self._source_code += "\n}\n"

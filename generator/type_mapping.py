from typing import List
from api import Class

class_set = dict()


def fill_class_set(classes: List[Class]):
    for class_ in classes:
        # TODO: CHECK IF THERE MIGHT BE A BUG WITH CLASSES LIKE PACKET
        class_set[class_.definition.name] = class_.definition.category.lower()


def map_type(typename):

    if '|null' in typename:
        typename = typename.replace('|null', '')

    if '&' in typename:
        typename = typename.replace('&', '')

    if typename in class_set:
        return typename

    match typename:
        case 'void':
            return 'void'
        case 'int':
            return 'Int'
        case 'float':
            return 'Float'
        case 'string':
            return 'String'
        case 'bool':
            return 'Bool'
        case 'userpointer':
            return 'SQUserPointer'
        case 'userdata':
            return 'SQUserPointer'
        case '{day, hour, min}':
            return 'GameTime'
        case '{x, y}':
            return 'Position2d'
        case '{x, y, z}':
            return 'Position3d'
        case '{width, height}':
            return 'Size2d'
        case '{x, y, width, height}':
            # TODO: FIX UV NAME COLLISION
            return 'Rect'
        case '{x, y, bpp}':
            return 'Resolution'
        case '{instance, amount, name}':
            return 'Item'
        case '{r, g, b}':
            return 'Color'
        case '{bodyModel, bodyTxt, headModel, headTxt}':
            return 'BodyVisual'
        case '{packetReceived, packetlossTotal, packetlossLastSecond, messagesInResendBuffer, messageInSendBuffer, ' \
             'bytesInResendBuffer, bytesInSendBuffer}':
            return 'NetworkStats'
        case '{name, x, y, z}':
            return 'Position3dWithName'
        case _:
            # TODO: HANDLE DEFAULT CASE BETTER
            return 'SQObject'

from datetime import datetime


class BaseGenerator:
    def __init__(self):
        self._header_code = str()
        self._source_code = str()

    @property
    def header_code(self):
        return self._header_code

    @property
    def source_code(self):
        return self._source_code

    def generate(self):
        time_now = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        intro_comment = f"/// NoNut generated: {time_now}\n" \
                        "/// This file is autogenerated from api.json provided by G2O Team\n" \
                        "/// Do not edit contents of this file as it may be overriden\n\n"

        self._header_code += intro_comment
        self._source_code += intro_comment

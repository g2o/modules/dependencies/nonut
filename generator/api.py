import dataclasses
from enum import Enum, StrEnum
from typing import Optional, List, Any
from dataclasses import dataclass


# @dataclass
# class TypeEnum(StrEnum):
#     ANY = "any"
#     ARRAY_NULL = "array|null"
#     ARRAY_USERPOINTER = "array[userpointer]"
#     BOOL = "bool"
#     B_BOX3_D = "BBox3d"
#     DAMAGE_DESCRIPTION = "DamageDescription"
#     EMPTY = "..."
#     FLOAT = "float"
#     FUNCTION = "function"
#     INT = "int"
#     INT_FLOAT_STRING = "int|float|string"
#     INT_STRING = "int|string"
#     ITEM_GROUND = "ItemGround"
#     MAT3 = "Mat3"
#     MAT4 = "Mat4"
#     PACKET = "Packet"
#     QUAT = "Quat"
#     STRING = "string"
#     USERDATA = "userdata"
#     USERPOINTER = "userpointer"
#     VEC2 = "Vec2"
#     VEC2_I = "Vec2i"
#     VEC3 = "Vec3"
#     VEC4 = "Vec4"
#     VOB = "Vob"
#     ZARRAY = "zarray&"


@dataclass
class Param:
    default: str
    description: str
    name: str
    obj_type: str

    def __init__(self, default: str, description: str, name: str, obj_type: str) -> None:
        self.default = default
        self.description = description
        self.name = name
        self.obj_type = obj_type

    @staticmethod
    def from_dict(obj: Any) -> 'Param':
        if obj is None:
            return None
        _default = str(obj.get("default"))
        _description = str(obj.get("description"))
        _name = str(obj.get("name"))
        _obj_type = str(obj.get("type"))
        return Param(_default, _description, _name, _obj_type)


@dataclass
class Constructor:
    declaration: str
    deprecated: str
    description: str
    notes: List[Any]
    params: List[Param]
    version: str

    def __init__(self, declaration: str, deprecated: str, description: str, notes: List[Any],
                 params: List[Param], version: str) -> None:
        self.declaration = declaration
        self.deprecated = deprecated
        self.description = description
        self.notes = notes
        self.params = params
        self.version = version

    @staticmethod
    def from_dict(obj: Any) -> 'Constructor':
        if obj is None:
            return None
        _declaration = str(obj.get("declaration"))
        _deprecated = str(obj.get("deprecated"))
        _description = str(obj.get("description"))
        _notes = [Any(y) for y in obj.get("notes", [])]
        _params = [Param.from_dict(y) for y in obj.get("params", [])]
        _version = str(obj.get("version"))
        return Constructor(_declaration, _deprecated, _description, _notes, _params, _version)


@dataclass
class Definition:
    category: str
    deprecated: str
    description: str
    extends: str
    name: str
    notes: List[str]
    side: str
    static: bool
    version: str

    def __init__(self, category: str, deprecated: str, description: str, extends: str,
                 name: str, notes: List[str], side: str, static: bool, version: str) -> None:
        self.category = category
        self.deprecated = deprecated
        self.description = description
        self.extends = extends
        self.name = name
        self.notes = notes
        self.side = side
        self.static = static
        self.version = version

    @staticmethod
    def from_dict(obj: Any) -> 'Definition':
        if obj is None:
            return None
        _category = str(obj.get("category"))
        _deprecated = str(obj.get("deprecated"))
        _description = str(obj.get("description"))
        _extends = str(obj.get("extends"))
        _name = str(obj.get("name"))
        _notes = [str(y) for y in obj.get("notes", [])]
        _side = str(obj.get("side"))
        _static = bool(obj.get("static"))
        _version = str(obj.get("version"))
        return Definition(_category, _deprecated, _description, _extends, _name, _notes, _side, _static, _version)


@dataclass
class Returns:
    description: str
    obj_type: str

    def __init__(self, description: str, obj_type: str) -> None:
        self.description = description
        self.obj_type = obj_type

    @staticmethod
    def from_dict(obj: Any) -> 'Returns':
        if obj is None:
            return None
        _description = str(obj.get("description"))
        _obj_type = str(obj.get("type"))
        return Returns(_description, _obj_type)


@dataclass
class Function:
    declaration: str
    deprecated: str
    description: str
    name: str
    notes: List[str]
    params: List[Param]
    returns: Returns
    static: bool
    version: str
    category: str
    side: str

    def __init__(self, declaration: str, deprecated: str, description: str, name: str, notes: List[str],
                 params: List[Param], returns: Returns, static: bool, version: str,
                 category: str, side: str) -> None:
        self.declaration = declaration
        self.deprecated = deprecated
        self.description = description
        self.name = name
        self.notes = notes
        self.params = params
        self.returns = returns
        self.static = static
        self.version = version
        self.category = category
        self.side = side

    @staticmethod
    def from_dict(obj: Any) -> 'Function':
        if obj is None:
            return None
        _declaration = str(obj.get("declaration"))
        _deprecated = str(obj.get("deprecated"))
        _description = str(obj.get("description"))
        _name = str(obj.get("name"))
        _notes = [str(y) for y in obj.get("notes", [])]
        _params = [Param.from_dict(y) for y in obj.get("params", [])]
        _returns = Returns.from_dict(obj.get("returns"))
        _static = bool(obj.get("static"))
        _version = str(obj.get("version"))
        _category = str(obj.get("category"))
        _side = str(obj.get("side"))
        return Function(_declaration, _deprecated, _description, _name, _notes, _params, _returns, _static, _version, _category, _side)


@dataclass
class Global:
    deprecated: str
    description: str
    name: str
    notes: List[str]
    read_only: bool
    returns: Returns
    version: str
    side: str

    def __init__(self, deprecated: str, description: str, name: str, notes: List[str],
                 read_only: bool, returns: Returns, version: str, side: str) -> None:
        self.deprecated = deprecated
        self.description = description
        self.name = name
        self.notes = notes
        self.read_only = read_only
        self.returns = returns
        self.version = version
        self.side = side

    @staticmethod
    def from_dict(obj: Any) -> 'Global':
        if obj is None:
            return None
        _deprecated = str(obj.get("deprecated"))
        _description = str(obj.get("description"))
        _name = str(obj.get("name"))
        _notes = [str(y) for y in obj.get("notes", [])]
        _read_only = bool(obj.get("read_only"))
        _returns = Returns.from_dict(obj.get("returns"))
        _version = str(obj.get("version"))
        _side = str(obj.get("side"))
        return Global(_deprecated, _description, _name, _notes, _read_only, _returns, _version, _side)


@dataclass
class Class:
    constructors: List[Constructor]
    definition: Definition
    methods: List[Function]
    properties: List[Global]

    def __init__(self, constructors: List[Constructor], definition: Definition, methods: List[Function],
                 properties: List[Global]) -> None:
        self.constructors = constructors
        self.definition = definition
        self.methods = methods
        self.properties = properties

    @staticmethod
    def from_dict(obj: Any) -> 'Class':
        if obj is None:
            return None
        _constructors = [Constructor.from_dict(y) for y in obj.get("constructors")]
        _definition = Definition.from_dict(obj.get("definition"))
        _methods = [Function.from_dict(y) for y in obj.get("methods", [])]
        _properties = [Global.from_dict(y) for y in obj.get("properties", [])]
        return Class(_constructors, _definition, _methods, _properties)


@dataclass
class Element:
    category: str
    deprecated: str
    description: str
    name: str
    side: str
    version: str

    def __init__(self, category: str, deprecated: str, description: str, name: str, side: str,
                 version: str) -> None:
        self.category = category
        self.deprecated = deprecated
        self.description = description
        self.name = name
        self.side = side
        self.version = version

    @staticmethod
    def from_dict(obj: Any) -> 'Element':
        if obj is None:
            return None
        _category = str(obj.get("category"))
        _deprecated = str(obj.get("deprecated"))
        _description = str(obj.get("description"))
        _name = str(obj.get("name"))
        _side = str(obj.get("side"))
        _version = str(obj.get("version"))
        return Element(_category, _deprecated, _description, _name, _side, _version)


@dataclass
class Constant:
    category: str
    elements: List[Element]
    side: str

    def __init__(self, category: str, elements: List[Element], side: str) -> None:
        self.category = category
        self.elements = elements
        self.side = side

    @staticmethod
    def from_dict(obj: Any) -> 'Constant':
        if obj is None:
            return None
        _category = str(obj.get("category"))
        _elements = [Element.from_dict(y) for y in obj.get("elements", [])]
        _side = str(obj.get("side"))
        return Constant(_category, _elements, _side)


@dataclass
class Event:
    cancellable: bool
    category: str
    declaration: str
    deprecated: str
    description: str
    name: str
    notes: List[str]
    params: List[Param]
    side: str
    version: str

    def __init__(self, cancellable: bool, category: str, declaration: str, deprecated: str,
                 description: str, name: str, notes: List[str], params: List[Param], side: str,
                 version: str) -> None:
        self.cancellable = cancellable
        self.category = category
        self.declaration = declaration
        self.deprecated = deprecated
        self.description = description
        self.name = name
        self.notes = notes
        self.params = params
        self.side = side
        self.version = version

    @staticmethod
    def from_dict(obj: Any) -> 'Event':
        if obj is None:
            return None
        _cancellable = bool(obj.get("cancellable"))
        _category = str(obj.get("category"))
        _declaration = str(obj.get("declaration"))
        _deprecated = str(obj.get("deprecated"))
        _description = str(obj.get("description"))
        _name = str(obj.get("name"))
        _notes: List[str] = [str(y) for y in obj.get("notes", [])]
        _params: List[Param] = [Param.from_dict(y) for y in obj.get("params", [])]
        _side = str(obj.get("side"))
        _version = str(obj.get("version"))
        return Event(_cancellable, _category, _declaration, _deprecated, _description, _name, _notes, _params, _side, _version)


@dataclass
class Api:
    classes: List[Class]
    constants: List[Constant]
    events: List[Event]
    functions: List[Function]
    globals: List[Global]

    def __init__(self, classes: List[Class], constants: List[Constant], events: List[Event], functions: List[Function],
                 globals: List[Global]) -> None:
        self.classes = classes
        self.constants = constants
        self.events = events
        self.functions = functions
        self.globals = globals

    @staticmethod
    def from_dict(obj: Any) -> 'Api':
        if obj is None:
            return None
        _classes = [Class.from_dict(y) for y in obj.get("classes", [])]
        _constants = [Constant.from_dict(y) for y in obj.get("constant", [])]
        _events = [Event.from_dict(y) for y in obj.get("events", [])]
        _functions = [Function.from_dict(y) for y in obj.get("functions", [])]
        _globals = [Global.from_dict(y) for y in obj.get("globals", [])]
        return Api(_classes, _constants, _events, _functions, _globals)
